<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Posts2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
	        //Schema::defaultStringLength ( 191 );
	        $table->increments ( 'id' );
	        $table->text  ( 'message' );
	        $table->string ( 'secret' );
	        //$table->text ( 'enc' );
	        $table->string ( 'post_id' )->unique ()->nullable();
	        $table->timestamps ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
