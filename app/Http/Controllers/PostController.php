<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;

class PostController extends Controller {

	/**
	 * Redirect to Home view
	 * @view
	 */

	public function home () {
		return view ( 'post.home' );
	}

	/**
	 * Get All data of Posts table
	 * @array with all data Posts table
	 */
	public function index () {
		$posts  = Post::all ();
		$carbon = Carbon::setLocale ( 'uk' );

		return view ( 'post.index', compact ( 'posts' ) );
	}

	/**
	 * @string
	 * Compare two input values
	 * @bool
	 */

	public function iSvalidSecret ( $reqKey, $secretDb ) {
		if ( $reqKey == $secretDb ) {
			return true;
		} else {
			echo 'Secret Key Is Not Correct!';
			Session::flash ( 'Fail', 'Secret Key Is Not Correct!' );

			return false;
		}
	}

	/**
	 * @id string
	 * @request object
	 * Compare & check user input secret key & secret key in DB which relate to this message
	 * @array post
	 */
	public function secret ( $id, Request $request ) {
		$reqKey   = $request->input ( 'secret' );
		$secretDb = DB::table ( 'posts' )->where ( 'post_id', '=', $id )->value ( 'secret' );
		if ( $this->iSvalidSecret ( $reqKey, $secretDb ) ) {
			$post = Post::where ( 'post_id', '=', $id )->get ();

			return view ( 'post.show', compact ( 'post' ) );
		}
	}

	/**
	 * @id string, id of Message
	 * Get & Show single Message from DB with proper id
	 * @array post
	 */
	public function show ( $id ) {
		$post = Post::where ( 'post_id', '=', $id )->get ();

		return view ( 'post.check', compact ( 'post' ) );
	}

	/**
	 * @request object
	 * validate user inpiut data(must be not Empty!)
	 * generate unique ID to each message
	 * Store date in DB in posts table
	 * @bool
	 */
	public function store ( Request $request ) {
		$this->validate ( $request, [
			'message' => 'required',
			'secret'  => 'required'
		] );

		$post          = new Post();
		$post->message = $request->message;
		$post->secret  = $request->secret;
		$post->post_id = uniqid ( 'pb', false );
		$post->save ();

		Session::flash ( 'success', 'Пост збережений' );

		return redirect ()->route ( 'posts' );
	}


	/**
	 * @id string
	 * @request object
	 * Find & delete message with proper id in DB
	 * @bool
	 */
	public function destroy ( $id ) {
		$post = Post::where ( 'post_id', '=', $id );
		$post->delete ();

		return redirect ()->back ();
	}

}
