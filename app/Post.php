<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Post extends Model {
	protected $table = 'posts';
	protected $fillable = [ 'message', 'secret', 'post_id' ];

	/**
	 * @value string
	 * Set data in Message column to encrypted string.
	 *
	 * @value string
	 */
	public function setMessageAttribute ( $value ) {
		$this->attributes['message'] = Crypt::encryptString ( $value );
	}

	/**
	 * @value string
	 * Retrieve data in Message column & decrypt to original string.
	 *
	 * @value string
	 */
	public function getMessageAttribute ( $value ) {
		return Crypt::decryptString ( $value );
	}
}
