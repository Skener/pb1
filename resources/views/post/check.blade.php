@extends ('layouts.app')
@section('content')
    @foreach($post as $p)
        <h4>Check Secret Key for Message with ID:{{$p->post_id}}</h4>
        <form action="{{route ('message.show.secret',['id'=>$p->post_id])}}" method="post">
            {{ csrf_field() }}
            <label for="secret">Secret key</label>
            <input type="text" name="secret">
            <input type="submit" name="submit" value="check secret key">
        </form>
    @endforeach
@endsection