@extends ('layouts.app')
@section('content')
    @foreach($post as $p)
        <h2>Message with ID:<i id="id">{{$p->post_id}}</i></h2>
        <p>
        </p>
        <p>
        <p>Content:</p>
        <mark><strong>{{$p->message}}</strong></mark>
        @endforeach
        </p>

        <script>
            $(document).ready(function () {
                //if page load, user see page, init ajax function to delete message by proper route with id
                setTimeout(function () {
                        var URL = $(location).attr('href');
                        url = URL.split('/');
                        var id = url[url.length - 1];
                        alert("Message from this url: http://localhost:8888/message/destroy/" + id + "  was deleted after 5 seconds!");
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: 'get',
                            cache: false,
                            data: {"id": id},
                            processData: false,
                            url: "http://localhost:8888/message/destroy/" + id,
                            {{--url:"{{ URL::route('post.delete')}}/"+id,--}}
                            success:
                                function (data) {
                                    console.log(data);
                                }
                        })
                    },

                    5000);
            });
        </script>
@endsection