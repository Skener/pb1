@extends ('layouts.app')
@section('content')
    <div class="col-sm-8">
        <h2>Your Messages</h2>
        <ul class="list-group">
            <table class='table table-bordered table-hover text-center'>
                <thead class="thead-dark">
                <tr>
                    <th class="text-center" scope="col">Original</th>
                    <th class="text-center" scope="col">Id</th>
                    <th class="text-center" scope="col">Secret</th>
                    <th class="text-center" scope="col">Url</th>
                    <th class="text-center" scope="col">Created</th>
                    <th class="text-center" scope="col">Destroy manually</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->message}}</td>
                        <td>{{ $post->post_id}}</td>
                        <td>{{$post->secret}}</td>
                        <td>
                            <a href="{{route('message.show',['id'=>$post->post_id])}}">
                                {{'/message/show/'.$post->post_id}}
                            </a>
                        </td>
                        <td>{{$post->created_at->diffForHumans()}}</td>
                        <td><a href="{{route('post.delete', ['id'=>$post->post_id])}}" class="btn btn-danger">Destroy</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </ul>
    </div>
@endsection
