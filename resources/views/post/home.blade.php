@extends ('layouts.app')
@section('content')
    <p></p>
    <p></p>
    <div class="form-group">
        <form action="{{route('posts.store')}}" method="post">
            {{ csrf_field() }}
            <label for="message">Your message</label>
            <input class="form-control" type="text" name="message">
            <label for="secret">Your secret key</label>
            <input class="form-control" type="password" name="secret">
            <input class="btn btn-outline-secondary form-control" type="submit" name="submit" value="Send">
        </form>
    </div>
@endsection
