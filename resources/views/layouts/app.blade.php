<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <title>{{ config('app.name', '') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">


    <link href='http://fonts.googleapis.com/css?family=Parisienne&subset=latin,cyrillic' rel='stylesheet'
          type='text/css'>

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">

    <nav class="navbar navbar-expand-lg navbar-light bg-light mt-5">
        <a class="navbar-brand" href="#">Strange app</a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a href="{{route ('home')}}" class="nav-link">Home</a></li>
            <li class="nav-item"><a href="{{route('posts')}}" class="nav-link">Messages</a></li>
        </ul>
    </nav>
    <div class="panel panel-default">
        @include('includes.errors')
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-2">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
{{--<script src="{{ asset('js/jquery3.js') }}"></script>--}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script>
    @if(Session::has('success'))
    toastr.success("{{Session::get('success')}}")
    @endif
</script>
</body>
</html>
