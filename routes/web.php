<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//lists of messages
Route::get ( '/posts', [
	'uses' => 'PostController@index',
	'as'   => 'posts'
] );
//front index start page
Route::get ( '/', [
	'uses' => 'PostController@home',
	'as'   => 'home'
] );
// store messages
Route::post ( 'posts/store', [
	'uses' => 'PostController@store',
	'as'   => 'posts.store'
] );
//show single message by id
Route::get ( 'message/show/{id}', [
	'uses' => 'PostController@show',
	'as'   => 'message.show'
] );
//check secret key
Route::post ( 'message/show/secret/{id}', [
	'uses' => 'PostController@secret',
	'as'   => 'message.show.secret'
] );
//delete message by id

Route::get ( 'message/destroy/{id}', [
	'uses' => 'PostController@destroy',
	'as'   => 'post.delete'
] );



